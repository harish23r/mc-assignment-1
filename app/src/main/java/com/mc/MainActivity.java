package com.mc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import com.mc.GraphView;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    GraphView graphView;
    Button startButton;
    Button stopButton;
    Random r;
    float[] rand_floats;

    int[] vertical_values = {500, 1000, 1500, 2000};
    int[] horizontal_values = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
    int horizontal_min = 0, horizontal_max = 100;
    int vertical_min = 0, getVertical_max = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        graphView = (GraphView)findViewById(R.id.graphView);
        startButton = (Button)findViewById(R.id.startButton);
        stopButton = (Button)findViewById(R.id.stopButton);

        vertical_values =
        horizontal_values

        // Generate random floats
        r = new Random();
        rand_floats = new float[20];
        for(int i = 0; i < rand_floats.length; i++) {
            rand_floats[i] = r.nextFloat();
        }

        // Onclick listener for start button
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Clicked on START");
            }
        });

        // Onclick listener for stop button
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Clicked on STOP");
            }
        });
    }
}
